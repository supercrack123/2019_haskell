-- Part 1: Recursive Functions

lconcat :: [[a]] -> [a]
lconcat l = case l of
                [] -> []
                (x:[]) -> x
                (x:xs) -> x ++ lconcat xs

lfoldl :: ((a, b) -> b) -> b -> [a] -> b
lfoldl f e l = case l of
                    [] -> e
                    (x:[]) -> f (x, e)
                    (x:xs) -> lfoldl f  (f (x, e)) xs

-- Part 2: Tail-Recursive Functions

fact :: Int -> Int
fact n =
  let
    factTR n result = if n == 0 then result else factTR (n-1) (n * result)
  in
    factTR n 1

power :: Int -> Int -> Int
power x n =
  let
    powerTR x n result = if n == 0 then result else powerTR x (n-1) (x * result)
  in
    powerTR x n 1

fib :: Int -> Int
fib n =
  let
    fibTR n a result = if n == 0 then result else fibTR (n-1) result (a + result)
  in
    fibTR n 0 1

lfilter :: (a -> Bool) -> [a] -> [a]
lfilter p l =
  let
    lfilterTR p l result = case l of
                                [] -> result
                                (x:xs) -> if (p x) then lfilterTR p xs (result ++ [x]) else lfilterTR p xs result
  in
    lfilterTR p l []

ltabulate :: Int -> (Int -> a) -> [a]
ltabulate n f =
  let
    ltabulateTR n f result = if n == 0 then result else ltabulateTR (n-1) f ( [f (n-1)] ++ result )
  in
    ltabulateTR n f []

union :: (Eq a) => [a] -> [a] -> [a]
union s t = case (s,t) of
                  ([],[]) -> []
                  ([],(y:ys)) -> t
                  ((x:xs),[]) -> union xs ([x] ++ t)
                  ((x:xs),(y:ys)) -> if x `elem` t then union xs t else union xs ([x] ++ t)

data Tree t = Leaf t | Node (Tree t, t, Tree t)

inorder :: Tree a -> [a]
inorder t =
  let
    inorderTR t remainder result = case t of
                                    Leaf n -> case remainder of 
                                                      [] -> result ++ [n]
                                                      (r:rs) -> inorderTR r rs (result ++ [n])
                                    Node (left, n, right) -> inorderTR left ((Leaf n):right:remainder) result
  in
    inorderTR t [] []


postorder :: Tree a -> [a]
postorder t =
  let
    postorderTR t remainder result = case t of
                                    Leaf n -> case remainder of 
                                                      [] -> [n] ++ result
                                                      (r:rs) -> postorderTR r rs ([n] ++ result)
                                    Node (left, n, right) -> postorderTR right (left:remainder) ([n] ++ result)
  in
    postorderTR t [] []


preorder :: Tree a -> [a]
preorder t =
  let
    preorderTR t remainder result = case t of
                                    Leaf n -> case remainder of 
                                                      [] -> result ++ [n]
                                                      (r:rs) -> preorderTR r rs (result ++ [n])
                                    Node (left, n, right) -> preorderTR left (right:remainder) (result ++ [n])
  in
    preorderTR t [] []

-- Part 3: Sorting

quicksort :: (Ord a) => [a] -> [a]
quicksort l = case l of
                  [] -> []
                  (x:xs) -> let smallsort = quicksort [small | small <- xs, small <= x]
                                bigsort = quicksort [big | big <- xs, big > x]
                            in smallsort ++ [x] ++ bigsort

mergesort :: (Ord a) => [a] -> [a]
mergesort l = case l of
                  [] -> []
                  (x:[]) -> [x]
                  (x:xs) -> merging (mergesort (take ((length l) `div` 2) l)) (mergesort (drop ((length l) `div` 2) l))
                            where
                              merging :: (Ord a) => [a] -> [a] -> [a]
                              merging n [] = n
                              merging [] m = m
                              merging (n:ns) (m:ms)
                                | n < m     = n:(merging ns (m:ms))
                                | otherwise = m:(merging (n:ns) ms)

-- Part 4: Heap

type Loc = (Int) -- to be defined by students
type Heap t = ([(t,Loc)]) -- to be defined by students

heapEmpty :: () -> Heap a
heapEmpty = return []

heapAllocate :: Heap a -> a -> (Heap a, Loc)
heapAllocate h v = (h', l)
                    where
                      h' = case h of
                                [] -> [(v,1)]
                                (x:xs) -> h ++ [(v, l)]
                      l = (getaddress h 0) + 1
                      getaddress :: Heap a -> Loc -> Loc
                      getaddress q addr = case q of
                                          [] -> addr
                                          ((n,m):xs) -> if m > addr then getaddress xs m else getaddress xs addr

heapDereference :: Heap a -> Loc -> Maybe a
heapDereference h l = case h of
                          [] -> Nothing
                          ((n,m):xs) -> if m == l then return n else heapDereference xs l 


check h l = case h of
                [] -> False
                ((n,m):xs) -> if m == l then True else check xs l

getHeap :: Heap a -> Loc -> a -> Heap a
getHeap h l v = case h of
                    [] -> []
                    ((n,m):xs) -> if m == l then (v,m):xs else (n,m):getHeap xs l v
                   

heapUpdate :: Heap a -> Loc -> a -> Maybe (Heap a)
heapUpdate h l v = if (check h l) 
                   then return (getHeap h l v)
                   else Nothing


-- DONE!

main = putStrLn "Load Complete!"
