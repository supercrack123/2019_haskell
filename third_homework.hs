-- 'TVar' as the type for variables
type TVar = String

-- 'Var' for variables
-- 'Abs' for λ-abstractions
-- 'App' for λ-applications
data Expr = Var TVar | Abs (TVar, Expr) | App (Expr, Expr) deriving (Show)

--------------------------------------------------------------------------------

-- FV(e)
getFreeVariables :: Expr -> [TVar]
getFreeVariables e = case e of
                          Var n -> [n]
                          Abs (n, e) ->  [x | x <- (getFreeVariables e), y <- [n], x /= y] 
                          App (e1, e2) -> union (getFreeVariables e1) (getFreeVariables e2) 
                                          where
                                            union :: [TVar] -> [TVar] -> [TVar]
                                            union [] l2 = l2
                                            union (h1:t1) l2 =
                                              if elem h1 l2 then
                                                 union t1 l2
                                              else
                                                 union t1 (l2 ++ [h1])

-- getFreshVariable
getFreshVariable :: TVar -> Expr -> Expr -> TVar
getFreshVariable varName e e' =
  let
    union :: [TVar] -> [TVar] -> [TVar]
    union [] l2 = l2
    union (h1:t1) l2 =
      if elem h1 l2 then
        union t1 l2
      else
        union t1 (l2 ++ [h1])

    getAllNames :: Expr -> [TVar]
    getAllNames (Var varName) = [varName]
    getAllNames (Abs (varName, e')) = union [varName] (getAllNames e')
    getAllNames (App (e1, e2)) = union (getAllNames e1) (getAllNames e2)

    isFreshVar :: TVar -> Bool
    isFreshVar [] = True
    isFreshVar (h:t) = (h == '\'') && isFreshVar t

    findLongestFreshVar :: TVar -> [TVar] -> TVar -> TVar
    findLongestFreshVar targetVarName [] longestFreshVar = longestFreshVar
    findLongestFreshVar targetVarName (h:t) longestFreshVar =
      let
        prefix  = take (length targetVarName) h
        postfix = drop (length targetVarName) h
      in
        if (prefix == targetVarName
            && isFreshVar postfix
            && length h > length longestFreshVar) then
          findLongestFreshVar targetVarName t h
        else
          findLongestFreshVar targetVarName t longestFreshVar
    
    allNames = union (getAllNames e) (getAllNames e')
  in
    (findLongestFreshVar varName allNames varName) ++ "'"

-- Substitution [e'/x]e
substitute :: (Expr, TVar) -> Expr -> Expr
substitute (e', x) e = case e of
                            Var y -> if x == y then e' else  Var y
                            App (e1, e2) -> App (substitute (e', x) e1, substitute (e', x) e2)
                            Abs (y, e) -> if x == y 
                                          then Abs (x, e) 
                                          else if y `elem` (getFreeVariables e') 
                                               then Abs (z, substitute (e', x) (translate e y z))
                                               else Abs (y, substitute (e', x) e)
                                        where
                                          z = getFreshVariable y e e'
  

translate e y z = case e of
                      Var x -> if x == y then Var z else (if x == z then Var y else e)
                      App (e1, e2) ->  App ((translate e1 y z), (translate e2 y z))
                      Abs (x, e) -> if x == y 
                                    then Abs (z, (translate e y z)) 
                                    else if x == z then Abs (y, (translate e y z)) else Abs (x, e)
                    

-- Call-by-Name
stepCallByName :: Expr -> Expr
stepCallByName expr = case expr of
                           Var x -> Var x 
                           Abs (x, e) -> Abs (x, e)
                           App (e1, e2) -> stepName e1 e2
                                          
stepName e1 e2 = case e1 of
                      Var x -> App (Var x, stepCallByName e2)
                      Abs (x, e) -> substitute (e2, x) e
                      App (e1', e2') -> App ((stepName e1' e2'), e2)


-- Call-by-Value
stepCallByValue :: Expr -> Expr
stepCallByValue expr = case expr of
                            Var x -> Var x
                            Abs (x, e) -> case e of
                                              App (e11, e22) -> Abs (x, (stepValue e11 e22))
                                              otherwise -> Abs (x, e)
                            App (e1, e2) -> stepValue e1 e2

stepValue e1 e2 = case e1 of
                      Var x -> App (Var x, stepCallByValue e2)
                      Abs (x, e) -> case e2 of
                                        Var y -> substitute (Var y, x) e
                                        Abs (y, e') -> substitute (Abs (y, e'), x) e
                                        otherwise -> App (Abs (x, e), stepCallByValue e2)
                      App (e1', e2') -> App ((stepValue e1' e2'), e2)
      
typecheck e = case e of
                  App (e1, e2) -> True
                  otherwise -> False
      
--------------------------------------------------------------------------------

main = putStrLn "Load Complete!"