-- example
addTwoInts :: Int -> Int -> Int
addTwoInts x y = x + y

sum_ :: Int -> Int
sum_ n = 
  if n == 0 then 0
  else n + (sum_ (n - 1))

fac :: Int -> Int
fac n = 
  if n == 1 then 1
  else n * (fac (n - 1))

fib :: Int -> Int
fib n = 
  if n == 0 then 1 
  else if n == 1 then 1
  else fib (n - 1) + fib (n - 2)

gcd_ :: Int -> Int -> Int
gcd_ m n = 
  if n == 0 then m 
  else gcd_ n (m `mod` n)

max_ :: [Int] -> Int
max_ l = case l of
              [] -> 0
              (x:[]) -> x
              (x:xs) -> if x > max_ xs then x else max_ xs 

data Tree t = Leaf t | Node (Tree t, t, Tree t)

sum_tree :: Tree Int -> Int
sum_tree t = case t of 
                  Leaf x -> x
                  Node (left, n, right) -> n + sum_tree (left) + sum_tree (right)

                      
depth :: Tree a -> Int
depth t = case t of
               Leaf x -> 0
               Node (left, n, right) -> max (depth left)(depth right) + 1
               
bin_search :: Tree Int -> Int -> Bool
bin_search t x = case t of
                      Leaf n -> if n == x then True else False
                      Node (left, n, right) -> if n == x then True else (if n > x then bin_search left x else bin_search right x)

preorder :: Tree a -> [a]
preorder t = case t of
                  Leaf n -> [n]
                  Node (left, n, right) -> [n] ++ (preorder left) ++ (preorder right)

list_add :: [Int] -> [Int] -> [Int]
list_add l1 l2 = case (l1,l2) of
                      ([],[]) -> []
                      ([],(y:ys)) -> [y] ++ list_add [] ys
                      ((x:xs),[]) -> [x] ++ list_add xs []
                      ((x:xs),(y:ys)) -> [x+y] ++ list_add xs ys
                

insert :: Int -> [Int] -> [Int]
insert m l = case l of
                  [] -> [m]
                  (y:[]) -> if m <= y then [m] ++ [y] else [y] ++ [m]
                  (y:ys) -> if m <= y then [m] ++ [y] ++ ys else [y] ++ insert m ys

insort :: [Int] -> [Int]
insort l = case l of
                [] -> []
                (y:ys) -> insert y (insort ys)


compose :: (a -> b) -> (b -> c) -> (a -> c)
compose f g = g.f

curry_ :: ((a, b) -> c) -> (a -> b -> c)
curry_ f = curry_temp f
  where
    curry_temp :: ((a, b) -> c) -> (a -> b -> c)
    curry_temp f x y = f (x, y)

uncurry_ :: (a -> b -> c) -> ((a, b) -> c)
uncurry_ f = uncurry_temp f
  where
    uncurry_temp :: (a -> b -> c) -> ((a, b) -> c)
    uncurry_temp f (x,y) = f x y


multifun :: (a -> a) -> Int -> (a -> a)
multifun f n = if (n == 1) then f else f.(multifun f (n-1))

ltake :: [a] -> Int -> [a]
ltake l n = case (l, n) of
                  ((x:xs), 0) -> []
                  ([], num) -> l
                  ((x:xs) , num) -> [x] ++ ltake xs (num - 1)

lall :: (a -> Bool) -> [a] -> Bool
lall f l = case (f, l) of
                (f, []) -> True
                (f, (x:xs)) -> if (f x) == True then lall f xs else False

lmap :: (a -> b) -> [a] -> [b]
lmap f l = case (f, l) of
                (f, []) -> []
                (f, (x:[])) -> [f x]
                (f, (x:xs)) -> [f x] ++ lmap f xs

lrev :: [a] -> [a]
lrev l = case l of
             [] -> []
             (x:xs) -> lrev xs ++ [x]

lzip :: [a] -> [b] -> [(a, b)]
lzip x y = case (x,y) of
                (manggu:manggus, []) -> []
                ([], nodab:nodabs) -> []
                (manggu:manggus, nodab:nodabs) -> [(manggu,nodab)] ++ lzip manggus nodabs

split :: [a] -> ([a], [a])
split l = (odd_ l,even_ l)
  where
    odd_ :: [a] -> [a]
    odd_ [] = []
    odd_ (x:[]) = [x]
    odd_ (x:y:ls) = [x] ++ odd_ ls
    even_ :: [a] -> [a]
    even_ [] = []
    even_ (x:[]) = []
    even_ (x:y:ls) = [y] ++ even_ ls


cartprod :: [a] -> [b] -> [(a, b)]
cartprod s t = [(x,y)| x <- s, y <- t]

main = putStrLn "Load Complete!"
